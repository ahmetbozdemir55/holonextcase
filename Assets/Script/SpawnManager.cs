﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager intance;

    public List<Transform> PlayerSpawnList;
    public List<Transform> OppentSpawnList;
    public GameObject PlayerPrefab;
    public GameObject OppenentPrefab;

    public GameObject Oppent;
    public GameObject Player;
    public GameObject mainCamera;

    public Transform _selectPlayerSpawnPoint;
    public Transform _selectOppentSpawnPoint;

    public GameObject _managerConChar;
    public GameObject _miniMap;

    // Start is called before the first frame update
    private void Awake()
    {
        intance = this;
        _selectPlayerSpawnPoint = PlayerSpawnList[Random.RandomRange(0, PlayerSpawnList.Count)];
        _selectOppentSpawnPoint = OppentSpawnList[Random.RandomRange(0, PlayerSpawnList.Count)];
        Player = Instantiate(PlayerPrefab, _selectPlayerSpawnPoint.position, _selectPlayerSpawnPoint.rotation);
        Oppent =  Instantiate(OppenentPrefab, _selectOppentSpawnPoint.position, _selectOppentSpawnPoint.rotation);

        Oppent.GetComponent<OppentChar>().enabled = true;
    }
    private void Start()
    {
        _managerConChar.GetComponent<managerConChar>().enabled = true;
        _miniMap.GetComponent<MiniMap>().enabled = true;

    }
    // Update is called once per frame
    void Update()
    {

    }
    //public void retryPlayerSpawn()
    //{
    //    mainCamera = GameObject.FindGameObjectWithTag("Camera");
    //    Transform _selectPlayerSpawnPoint = PlayerSpawnList[Random.RandomRange(0, PlayerSpawnList.Count)];
    //    Player = Instantiate(PlayerPrefab, _selectPlayerSpawnPoint.position, _selectPlayerSpawnPoint.rotation);
    //    _managerConChar.GetComponent<managerConChar>().enabled = true;
    //    _miniMap.GetComponent<MiniMap>().enabled = true;
    //    mainCamera.GetComponent<CameraMove>().camMove();
    //}
    //public void retryOppentSpawn()
    //{
    //    Transform _selectOppentSpawnPoint = OppentSpawnList[Random.RandomRange(0, PlayerSpawnList.Count)];
    //    Oppent = Instantiate(OppenentPrefab, _selectOppentSpawnPoint.position, _selectOppentSpawnPoint.rotation);
    //}
}
