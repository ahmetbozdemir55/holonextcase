﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public enum GameStatus
{
    menü,
    start,
    finish,
    exit
}

public class GameManager : MonoBehaviour
{
    public int Level;
    //public fireman ballController;
    public GameStatus gameStatus;
    void Awake()
    {
        gameStatus = GameStatus.menü;
    }
    public void Start()
    {

    }

    public void StartGame()
    {
        Invoke("Delay", 1f);
    }
    public void Delay()
    {
        gameStatus = GameStatus.start;

    }
    public void ClickExit()
    {
        gameStatus = GameStatus.exit;
    }
    public void GameFinish()
    {
        gameStatus = GameStatus.finish;

    }
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }
    // Update is called once per frame
    void Update()
    {

    }

}
