﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    public float healty=1f;

    public GameObject _managerConChar;
    public GameObject _miniMap;
    public GameObject mainCamera;
    public GameObject _spawnManager;
    public bool dead = false;
    public GameObject Oppent;
    public float playerSkor=0;
    // Start is called before the first frame update
    void Start()
    {
        Oppent = GameObject.FindGameObjectWithTag("Opponent");
        _managerConChar = GameObject.FindGameObjectWithTag("Manager");
        _miniMap = GameObject.FindGameObjectWithTag("miniMap");
        _spawnManager = GameObject.FindGameObjectWithTag("Manager");
    }

    // Update is called once per frame
    void Update()
    {
        if (dead=true && healty<=0)
        {
            dead = false;
            healty = 1;
            _managerConChar.GetComponent<managerConChar>().enabled = false;
            ResartPos();
            Invoke("PlayerDelay", 0.2f);
        }
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Bomb")
        {
            TakeDamage();
        }
    }
    void TakeDamage()
    {
        healty = healty - 0.2f;
    }
    public void ResartPos()
    {
        dead = true;

        Vector3 pos = _spawnManager.GetComponent<SpawnManager>()._selectPlayerSpawnPoint.position;
        _spawnManager.GetComponent<managerConChar>().lookDir = _spawnManager.GetComponent<SpawnManager>()._selectPlayerSpawnPoint.position;
        transform.position = _spawnManager.GetComponent<SpawnManager>()._selectPlayerSpawnPoint.position;
        Oppent.transform.position = _spawnManager.GetComponent<SpawnManager>()._selectOppentSpawnPoint.position;
        //_managerConChar.GetComponent<managerConChar>().enabled = true;
        Oppent.GetComponent<OppentChar>().oppenetScore = Oppent.GetComponent<OppentChar>().oppenetScore + 1;
        if (Oppent.GetComponent<OppentChar>().oppenetScore == 10)
        {
            Debug.Log("girdi");
            UIManager.instance.GameFinish();
        }

    }
    public void PlayerDelay()
    {
        _managerConChar.GetComponent<managerConChar>().enabled = true;
    }
}
