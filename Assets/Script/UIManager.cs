﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public GameManager gameManager;
    public GameObject _managerConChar;
    public GameObject _spawanManager;
    public GameObject _miniMap;

    //public CounterManager counterManager;
    public GameObject _player;
    public GameObject _opponent;

    [Header("Panels")]
    public GameObject MenüPanel;
    public GameObject GamePanel;
    public GameObject FinishPanel;
    [Header("UI References")]
    [SerializeField] private Image Fill_Image;
    public TextMeshProUGUI playerscoreText;
    public TextMeshProUGUI opponentscoreText;

    public void Awake()
    {
        instance = this ;
    }
    public void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _opponent = GameObject.FindGameObjectWithTag("Opponent");

    }

    public void Update()
    {
        if (gameManager.gameStatus != GameStatus.start)
        {
            return;
        }
        float newProgressValue = Mathf.InverseLerp(0, 1, _player.GetComponent<Player>().healty);
        UbdateFillmage(newProgressValue);
        playerscoreText.text = ""+_player.GetComponent<Player>().playerSkor;
        opponentscoreText.text = "" + _opponent.GetComponent<OppentChar>().oppenetScore;


    }

    private void UbdateFillmage(float value)
    {
        Fill_Image.fillAmount = value;
    }
    public void GameStart()
    {
        //Debug.Log("uı gelmiyor");
        HideAllPanel();
        GamePanel.gameObject.SetActive(true);



        gameManager.StartGame();

        _spawanManager.GetComponent<SpawnManager>().enabled = true;
        //_managerConChar.GetComponent<managerConChar>().enabled = true;
        //_miniMap.GetComponent<MiniMap>().enabled = true;
    }
    public void GameFinish()
    {
        HideAllPanel();
        FinishPanel.gameObject.SetActive(true);

        gameManager.GameFinish();
    }

    public void RestartGame()
    {
        HideAllPanel();

        gameManager.RestartGame();

    }
    public void GameExit()
    {
        Application.Quit();
    }
    public void HideAllPanel()
    {
        MenüPanel.gameObject.SetActive(false);
        GamePanel.gameObject.SetActive(false);
        FinishPanel.gameObject.SetActive(false);
    }

}
