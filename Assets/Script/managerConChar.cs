﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class managerConChar : MonoBehaviour
{
    ////LookCamera
    //public FixedTouchField fixedTouch;
    //protected float CameraAngle;
    //protected float CameraAngleSpeed=0.2f;
    public GameManager gameManager;
    public GameObject _meshPlayer;
    public Vector3 distance;
    public Vector3 lookDir;
    // component

    private CharacterController charController;
    private Animator animator;
    public managerJoystick mngrJoyStick;
    //Transform

    private Transform meshPlayer;

    //move
    private float inputX;
    private float inputZ;
    private Vector3 v_movement;
    private float moveSpeed;
    private float graviity;

    //Attack Player
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    public GameObject projectile;
    public GameObject attackpointerpos;

    //States
    public bool isDead;
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;

    // Start is called before the first frame update
    void Start()
    {
        moveSpeed = 0.1f;
        graviity = 0.5f;
        _meshPlayer = GameObject.FindGameObjectWithTag("meshPlayer");
        GameObject tempPlayer = GameObject.FindGameObjectWithTag("Player");
        attackpointerpos = GameObject.FindGameObjectWithTag("attackPoint");
        meshPlayer = tempPlayer.transform.GetChild(0);
        charController = tempPlayer.GetComponent<CharacterController>();
        animator = meshPlayer.GetComponent<Animator>();
        //mngrJoyStick = GameObject.Find("ımage_JoystickBg").GetComponent<managerJoystick>();
        
        
        //distance = tempPlayer.transform.position - mainCamera.transform.position;
        //_mainCamera.transform.DOMove(distance, 0.7f);
        //Debug.Log(distance);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.gameStatus != GameStatus.start)
        {
            return;
        }

        //inputX = Input.GetAxis("Horizontal");
        //inputZ = Input.GetAxis("Vertical");
        inputX = mngrJoyStick.inputHorizontal();
        inputZ = mngrJoyStick.inputVertical();
        if (inputX==0 & inputZ==0)
        {
            animator.SetBool("isRun", false);
        }
        else
        {
            animator.SetBool("isRun", true);
        }
        //if (Input.GetButtonDown("Fire1"))
        //{

        //    animator.SetBool("attack", true);
        //    AttackPlayer();
        //}
        //if (Input.GetButtonUp("Fire1"))
        //{
        //    animator.SetBool("attack",false);
        //}
        //CameraAngle += fixedTouch.TouchDist.x * CameraAngleSpeed;
        //Camera.main.transform.position = transform.position + Quaternion.AngleAxis(CameraAngle, Vector3.up) * new Vector3(0, 3, 4);
        //Camera.main.transform.rotation = Quaternion.LookRotation(transform.position+ Vector3.up*2f-Camera.main.transform.position, Vector3.up);
    }
    private void FixedUpdate()
    {
        //Gravity
        if (charController.isGrounded)
        {
            v_movement.y = 0f;
        }
        else
        {
            v_movement.y -= graviity * Time.deltaTime;
        }
        //movement
        v_movement = new Vector3(inputX*moveSpeed, v_movement.y, inputZ*moveSpeed);
        charController.Move(v_movement);
        // mesh transform radius
        if (inputX !=0||inputZ!=0)
        {
            lookDir = new Vector3(v_movement.x, 0, v_movement.z);
            meshPlayer.rotation = Quaternion.LookRotation(lookDir);
        }
    }
    private void AttackPlayer()
    {
        if (isDead) return;


        transform.LookAt(_meshPlayer.transform);

        if (!alreadyAttacked)
        {

            //Attack
            ///Attack code here
            Rigidbody rb = Instantiate(projectile, attackpointerpos.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(_meshPlayer.transform.forward * 32f, ForceMode.Impulse);
            rb.AddForce(_meshPlayer.transform.up * 8f, ForceMode.Impulse);
            ///End of attack code

            alreadyAttacked = true;
            Invoke("ResetAttack", timeBetweenAttacks);
        }
    }
    private void ResetAttack()
    {
        if (isDead) return;

        alreadyAttacked = false;
    }
    public void Attack()
    {
        //animator.SetBool("attack", false);
        animator.SetBool("attack", true);
        AttackPlayer();
    }
}
