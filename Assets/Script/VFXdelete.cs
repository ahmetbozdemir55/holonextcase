﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXdelete : MonoBehaviour
{
    void Start()
    {
        Invoke("Delay",1f);
    }
    public void Delay()
    {
        Destroy(gameObject);
    }
}
