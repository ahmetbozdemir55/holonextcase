﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granede : MonoBehaviour
{

    [SerializeField] GameObject exploParticle;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Obstacle" || collision.transform.tag == "Opponent" || collision.transform.tag == "Player")
        {

            Explode();
        }
    }
    void Explode()
    {
        Instantiate(exploParticle, transform.position, transform.rotation);

    }
}
