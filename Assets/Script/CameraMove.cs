﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{

    public Transform camTarget;
    public float plerp = 5f;
    public float rlerp = .01f;
    public GameObject meshPlayer;


    void Start()
    {
        camMove();
    }
    public void camMove()
    {
        camTarget = GameObject.FindGameObjectWithTag("CamTarget").transform;
        meshPlayer = GameObject.FindGameObjectWithTag("meshPlayer");
        transform.position = Vector3.Lerp(transform.position, camTarget.position, plerp);
        transform.SetParent(meshPlayer.transform);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
